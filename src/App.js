import logo from './logo.svg';
import './App.css';
import Test from "./components/Test";
import Greetings from "./components/Greetings";
import UserInfo from "./components/UserInfo";
import UserInfoWithFn from "./components/UserInfoWithFn";
import OnOff from "./components/OnOff";


function App() {

  const user = {
    name : "Jesus"
  };

  const userfn = userData => {
      console.log("Hello " + userData.name + " (Function)");
  };

  const turnOnTurnOff = () => {

  };

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />

        <Test />
        <Greetings name = "Jesus"/>
        <Greetings name = "Ana"/>
        <UserInfo user = {user}/>
        <UserInfoWithFn user={user} userfn={userfn}/>
        <OnOff />

        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
