import React from "react";

export default function UserInfoWithFn(props) {
    const userData = props.user;
    const userFunction = props.userfn;
    return (
        <div>
            <button onClick = {() => userFunction(userData)}>Message in Console</button>
        </div>
    )
}