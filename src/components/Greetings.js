import React from "react";

export default function Greetings(props) {
    return (
        <div>
            <h2>Hello {props.name}</h2>
        </div>
    )
}