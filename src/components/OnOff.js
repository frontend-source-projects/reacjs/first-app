import React, { useEffect, useState } from "react";

export default function OnOff() {
    const [state, setState] = useState(false);
    const [clicks, setClicks] = useState(0);

    useEffect(() => {
        console.log("Clicks: " + clicks);
    }, [clicks])
    
    const onOff = () => {
        //setState(!state);
        setState(prevValue => !prevValue);
        setClicks(clicks + 1);
    }

    return (
        <div>
            <h3>Turn [{state ? "On" : "Off"}]</h3>
            <button onClick = {() => onOff()}>On / Off</button>
            <h4>Clicks -{clicks}-</h4>
        </div>
    )
}